#include<iostream>
#include<string>
#include<fstream>
#include<vector>
#include<cstring>
#include<algorithm>
using namespace std;

typedef pair<int, int*> PII;

const int N = 2010;
int sentence_len = 50;

bool debug = false;

PII record[2][N];//dp算法的记事本，可以优化成一维
int len;
int best_value = -2;
int best_boardnum = 0;
int* best_boardpos;
bool flag = true;//表示还没有找到最佳分割
string str;
string str2;
int test_len = 20;//用于分句的测试长度

int quality(string s);
void dp(string s, int board_num);
int sentence_quality(int board_num, int* board_pos);
void readWordsLibrary();

void reset(){//恢复有关dp的全局变量初始值
	best_value = -2;
	best_boardnum = 0;
	best_boardpos = NULL;
	flag =  true;
}











//节点类
/*
class Node{
public:
	string word;//单词
	int freq;//词频
	Node *next;
	Node();//无参构造函数
	Node(string w, int f){//两参构造函数
		word=w;
		freq=f;
		next=NULL;
	}
	~Node(){//析构函数
		//if(next)
		delete next;
	}
};
*/

struct Node{

	string word;
	int freq;

};

//链表类
class List{
private:
	//Node *head;//头指针
	vector<Node> mywords;
	int count;//结点计数
public:
	List();//构造函数
	~List();//析构函数
	void insert(int position, string word, int freq);//在position位插入新节点
	void print();//打印链表
	void sortList();//排序
	int search(string str);//在链表中搜索str，若找不到则返回其合适的插入位置pos，找到则返回-1
	int search2(string str);


};

bool cmp(Node n1, Node n2){
	//strcmp(n1.word, n2.word)
	//string s;
	return (n1.word.compare(n2.word) < 0);
	//return n1.word > n2.word;
}

void List::sortList(){
	sort(mywords.begin(), mywords.end(), cmp);
}

void List::print(){
	for(int i = 0; i < count; i ++){
		cout<<mywords[i].word<<endl;
	}
}

List::List(){
	//构造头指针对应结点
	//Node *newNode=new Node("0", 0);
	//head=newNode;
	count=0;
}

List::~List(){
	/*
	Node *p=head, *q=p->next;
	//析构p对应的结点，通过q找到剩下还未析构的链表
	while(q){
		p->~Node();
		p=q;
		q=q->next;
		count--;
	}
	*/
}

int List::search(string str){
	//若该链表中还不存在除头结点以外的结点，直接返回插入位置0
	//if(!head->next) return 0;
	if(count == 0)return 0;
	int i = 0;
	if(mywords[i].word == str){
		mywords[i].freq ++;
		return -1;
	}
	for(i = 1; i <count; i ++){
		if(mywords[i].word == str){
			mywords[i].freq ++;
			return -1;
		}
		else if(mywords[i - 1].word < str && mywords[i].word > str){
			return i;
		}
	}
	return i;

	/*
	Node *p=head->next, *q=head;
	int pos=0;
	while(p){
		//如果找到了
		if(p->word==str){
			p->freq++;
			return -1;
		}
		//如果出现介于两者之间的情况，由于该链表是按字典序排列的，必然不存在目标单词
		else if(p->word>str && q->word<str){
			return pos;
		}
		p=p->next;
		q=q->next;
		pos++;
	}
	return pos;
	*/
}

int List::search2(string str){
	//若该链表中还不存在除头结点以外的结点，直接返回插入位置0
	//if(!head->next) return 0;
	if(count == 0)return 0;
	int i = 0;
	if(mywords[i].word == str){
		//mywords[i].freq ++;
		return -1;
	}
	for(i = 1; i <count; i ++){
		if(mywords[i].word == str){
			//mywords[i].freq ++;
			return -1;
		}
		else if(mywords[i - 1].word < str && mywords[i].word > str){
			return i;
		}
	}
	return i;
}

void List::insert(int position, string word, int freq){
	//新申请结点空间
	//Node *newNode=new Node(word, freq);
	//如果要插入到第一位
	/*
	if(position==0){
		//newNode->next=head->next;
		//head->next=newNode;
		
	}
	//如果插入位置非第一位
	else{
		Node *p=head;
		for(int i=0; i<position; i++){
			p=p->next;
		}
		newNode->next=p->next;
		p->next=newNode;
	}
	*/
	Node n;
	n.word = word;
	n.freq = freq;
	mywords.insert(mywords.begin() + position, n);
	//勿忘
	count++;
}

//解决方法类
class solution{
public:
	List letterList[26];//储存处理完毕的单词及其词频
	List stopList[26];//储存停词表

	void implement();//主操作函数 完成单词（字符串）的读入、处理、查找、比对停词表和存入链表
	void initialStopList();//初始化停词表链表
	bool isLetter(char c);//判断该字符是否为字母，是则返回true,否则返回false
	bool islowercase(char c);//判断该字符是否为小写字母，是则返回true,否则返回false
	void sortLists();//将26个链表分别排序
	void print();//打印链表内容
};

solution sol;

bool solution::islowercase(char c){
	if(c>='A' && c<='Z')
	{
		return false;
	}
	return true;
}

bool solution::isLetter(char c){
	if(c>='a' && c<='z' || c>='A' && c<='Z')
		return true;
	return false;
}

void solution::sortLists(){
	for(int i=0; i<26; i++){
		letterList[i].sortList();	
	}
}

//
void solution:: implement(){
	string tmp;
	//"D:\\20NKUwzr\\学习\\大二下\\算法导论\\大作业\\resources\\dictionarycase1-1.txt"
	//ifstream in("D:\\20NKUwzr\\学习\\大二下\\算法导论\\大作业\\resources\\dictionary2.txt");
	ifstream in(".\\dictionary2.txt");
	while(!in.eof()){
		//按词读入文件
		in>>tmp;
		//移除非字母的字符，同时将大写转小写
		for(int i=0; i<tmp.length(); i++){
			//如果这一位不是字母且不是连字符'-'
			if(!isLetter(tmp[i])  && tmp[i]!='-'){
				tmp.erase(i, 1);
				//如果经过移除，移除位之后的每个字符下标-1，需要退一位继续遍历
				i--;
			}
			//如果这一位是字母
			else if(!islowercase(tmp[i]))
				tmp[i]+=32;
		}
		//如果字符串已空，直接进行下一次读入
		if(tmp=="")continue;
		//如果字符串第一位或最后一位出现连字符，将其删去
		if(tmp[0]=='-') tmp.erase(0, 1);
		if(tmp[tmp.length()-1]=='-')tmp.erase(tmp.length()-1, 1);
		//如果除去连字符后字符串已空，直接进行下一次读入
		if(tmp=="")continue;
		//比对停词表
		//int flag=stopList[tmp[0]-97].search(tmp);
		int pos=letterList[tmp[0]-97].search(tmp);
		//如果在停词表中没有找到该词，满足插入第一条件
		//if(flag!=-1)
			//如果在对应首字母链表中也没有找到该词，即以前没有见过该词，满足插入第二条件，执行插入
			if(pos!=-1)
				letterList[tmp[0]-97].insert(pos, tmp, 1);
	}
	//记得关闭文件
	in.close();
}

void solution::initialStopList(){
	string tmp2;
	ifstream in2("D:\\20NKUwzr\\学习\\大二下\\算法导论\\大作业\\resources\\Stoplist.txt");
	while(!in2.eof()){
		in2>>tmp2;
		int pos=stopList[tmp2[0]-97].search(tmp2);
		//若没有在当前停词表中找到该词，即执行插入（疑似多余判断，但对pos，即插入位置的确定不是多余操作）
		if(pos!=-1)
		stopList[tmp2[0]-97].insert(pos, tmp2, 1);
	}
	in2.close();
}

void solution::print(){
	for(int i=0; i<26; i++){
		letterList[i].print();
	}
}














void divide_sentences();

string *sentences;
int sentence_size;

int main(){

	ofstream out;
	//out.open("D:\\20NKUwzr\\学习\\大二下\\算法导论\\大作业\\output3.txt");
	out.open(".\\output.txt");

	//out<<"abcde";
	//out<<"fghij";

	//读入词库
	readWordsLibrary();
	//readWordsLibrary("");

	/*
	//测试使用
	while(1){
		cout<<"input the word you want to select"<<endl;
		string s;
		cin>>s;
		//int pos=sol.letterList[s[0]-97].search2(s);
		//cout<<(pos==-1)<<endl;
		cout<<quality(s)<<endl;
	}
	*/

	//输入 限长2000
	//string str;
	cout<<"start input"<<endl;

	//cin>>str;
	//len = str.length();
	/**/
	string tmp2;
	//ifstream in2("D:\\20NKUwzr\\学习\\大二下\\算法导论\\大作业\\case2.txt");
	ifstream in2(".\\case2.txt");
	while(!in2.eof()){
		in2>>tmp2;
		str += tmp2;
	}
	in2.close();

	/**/

	len = str.length();
	for(int i = 0; i < len; i ++){
		cout<<str[i];
	}
	cout<<endl;
	
	str2 = str;

	cout<<"input complete, len = "<<len<<endl;

	sentence_size = len / sentence_len + 1;
	sentences = new string[sentence_size];

	divide_sentences();

	str = str2;

	cout<<"============================================================================================"<<endl;
	
	
	
	for(int h = 0; h < sentence_size; h ++){
		str = sentences[h];//因调适暂时注释
		len = str.length();

	

	//预处理
	for(int i = 0; i < len; i ++){
		if(str[i] >= 'A' && str[i] <= 'Z'){
			str[i] += 32;
		}
		if(str[i] >= 'a' && str[i] <= 'z'){
			//str.erase(i, 1);
			//i--;
		}
	}

	//dp算法
	for(int i = 1; i < len && flag; i ++){
		dp(str, i);//board_num >= 1
	}

	//输出
	cout<<"best_value = "<<best_value<<endl;
	for(int i = 0; i < best_boardnum; i ++){
		cout<<best_boardpos[i]<<" ";
	}
	cout<<endl;
	int st = 0;
	int fn = best_boardpos[0];
	for(int i = 1; i <= best_boardnum; i ++){
		for(int j = st; j <= fn; j ++){
			cout<<str[j];
			out<<str[j];
			
		}
		cout<<" ";
		out<<" ";
		st = fn + 1;
		fn = best_boardpos[i];
	}
	for(int j = st; j < len; j ++){
		cout<<str[j];
		out<<str[j];
		//cout<<" ";
		//out<<" ";
	}
	cout<<endl;
	out<<" ";
	if(h * sentence_len % 100 == 0){
		out<<endl;
	}

	reset();

	}
	out.close();
return 0;
}

int quality(string s){
	if(s == "")return 0;
	int pos=sol.letterList[s[0]-97].search2(s);
	//cout<<(pos==-1)<<endl;
	
	//if(s == "meet" || s == "at" || s == "eight" || s == "me"){
	//if(s == "the" || s == "youth" || s == "event" || s == "they" || s == "you" || s == "out"){
	if(pos==-1){
		return 1;
	}
	//else if(s == "")return 0;
	else return -1;
}

int sentence_quality(int board_num, int* board_pos){
	int sum = 0;
	string *strs = new string[board_num + 1];
	int st = 0;
	int fn = board_pos[0];
	for(int i = 0; i < board_num + 1; i ++){
		strs[i] = str.substr(st, fn - st + 1);
		st = fn + 1;
		fn = board_num>1?board_pos[i + 1]:(len - 1);
	}
	for(int i = 0; i < board_num + 1; i ++){
		sum += quality(strs[i]);
	}
	return sum;
}




void dp(string s, int board_num){
	cout<<"board_num = "<<board_num<<endl;
	//record[board_num - 1][i]
	if(board_num == 1){
		for(int i = 0; i < len - 1; i ++){//len个字符，只有len - 1个可以插板的位置
			record[0][i].first = sentence_quality(board_num, &i);
			int *bp = new int(i);
			record[0][i].second = bp;

			if(debug){
				cout<<"i = "<<i<<endl;
				cout<<"record[board_num - 1][i].first"<<endl;
				cout<<record[0][i].first<<endl;
				cout<<"record[board_num - 1][i].second"<<endl;
				for(int m = 0; m < board_num; m ++){
					cout<<record[0][i].second[m]<<" ";
				}
				//cout<<record[board_num - 1][i].second<<endl;
			}

			if(record[0][i].first > best_value){
				best_value = record[0][i].first;
				best_boardnum = board_num;
				best_boardpos = record[0][i].second;
				if(best_value == board_num + 1){
					flag = false;
					return;
				}
			}
		}
	}
	else{
		int *board_pos = new int[board_num];
		//int *myboard_pos = new int[board_num];
		for(int i = 0; i < len - 1; i ++){
			int *myboard_pos = new int[board_num];
			//预先算出来一个值作为max参照
			board_pos[0] = i;
			myboard_pos[0] = i;//两个pos做完全相同的处理
			for(int k = 0; k < board_num - 1; k ++){
				board_pos[k + 1] = record[0][0].second[k];
				myboard_pos[k + 1] = board_pos[k + 1];
			}
			//int max = record[board_num - 2][0].first;
			int max = sentence_quality(board_num, board_pos);
			
			if(debug){
				cout<<"print record of last line"<<endl;
				for(int d = 0; d < len - 1; d ++){
					for(int d2 = 0; d2 < board_num - 1; d2 ++){
						cout<<record[0][d].second[d2]<<" ";
					}
					cout<<endl;
				}
			}

			for(int j = 0; j < len - 1; j ++){//遍历前一行
				//获取前一行某个元素的隔板位置列表
				for(int k = 0; k < board_num - 1; k ++){
					board_pos[k + 1] = record[0][j].second[k];
				}
				//计算
				int value = sentence_quality(board_num, board_pos);
				if(value > max){
					max = value;
					for(int k = 0; k < board_num; k ++){
						myboard_pos[k] = board_pos[k];
					}
				}
			}
			record[1][i].first = max;
			record[1][i].second = myboard_pos;
			if(debug){
				cout<<"i = "<<i<<endl;
				cout<<"record[board_num - 1][i].first"<<endl;
				cout<<record[1][i].first<<endl;
				cout<<"record[board_num - 1][i].second"<<endl;
				for(int m = 0; m < board_num; m ++){
					cout<<record[1][i].second[m]<<" ";
				}
				//cout<<record[board_num - 1][i].second<<endl;
			}
			if(record[1][i].first > best_value){
				best_value = record[1][i].first;
				best_boardnum = board_num;
				best_boardpos = record[1][i].second;
				if(best_value == board_num + 1){
					flag = false;
					return;
				}
			}
		}
		//调换
		for(int i = 0; i < len - 1; i ++){
			record[0][i].first = record[1][i].first;
			record[0][i].second = record[1][i].second;
		}
	}
}



void readWordsLibrary(){
	//solution sol; 
	sol.implement();
	sol.sortLists();
	//sol.print();
}

void divide_sentences(){
	//现在str是很长的一篇文章，要把它切成很多句子
	//每个句子的预期长度是sentence_len
	int my_st = 0;
	int my_fn = 0;
	for(int h = 0; h < sentence_size - 1; h ++
		){
		my_st = my_fn + 1;
		int ss = h * sentence_len + sentence_len - test_len / 2;
		string sub = str2.substr(ss, test_len);
		//string sub = str.substr(my_st, test_len);
		str = sub;
		//下面对sub进行动态规划算法
		//=================================================================================

		len = sub.length();

		//预处理
		for(int i = 0; i < sub.length(); i ++){
			if(str[i] >= 'A' && str[i] <= 'Z'){
				str[i] += 32;
			}
			if(str[i] >= 'a' && str[i] <= 'z'){
				//str.erase(i, 1);
				//i--;
			}
		}

		//dp算法
		for(int i = 1; i < sub.length() && flag; i ++){
			dp(str, i);//board_num >= 1
		}

		int break_index = 0;//sub的index

		//输出
		cout<<"best_value = "<<best_value<<endl;
		for(int i = 0; i < best_boardnum; i ++){
			cout<<best_boardpos[i]<<" ";
		}
		cout<<endl;
		int st = 0;
		int fn = best_boardpos[0];
		int max_len = 1;
		for(int i = 1; i <= best_boardnum; i ++){
			string subsub = str.substr(st, fn - st + 1);
			if(quality(subsub) == 1){
				if(i == 1 || subsub.length() > max_len){
					max_len = subsub.length();
					break_index = st - 1;
					my_fn = (h + 1) * sentence_len - test_len / 2 + break_index;
				}
				//break;
			}
			for(int j = st; j <= fn; j ++){
				cout<<str[j];
			}
			cout<<" ";
			st = fn + 1;
			fn = best_boardpos[i];
		}
		for(int j = st; j < len; j ++){
			cout<<str[j];
		}
		cout<<endl;

		//=================================================================================

		//接下来遍历隔板之间的词，希望找到一个完整的词，并从此分割这两句话
		//即找到break_index
		//int mylen = sentence_len - (test_len / 2 - break_index);
		//int offset = (test_len / 2 - my_st);
		//sentences[i] = str.substr(i * sentence_len - offset, mylen);
		//my_st = break_index;//下一句的开始就是这一句的结尾
		sentences[h] = str2.substr(my_st, my_fn - my_st + 1);

		//重置dp相关的全局变量
		reset();
	}
	sentences[sentence_size - 1] = str2.substr(my_fn+1);

	//补丁处理
	sentences[0].insert(sentences[0].begin(), str2[0]);

	cout<<"print sentences"<<endl;
	for(int i = 0; i < sentence_size; i ++){
		for(int j = 0; j < sentences[i].size(); j ++){
			cout<<sentences[i][j];
		}
		cout<<endl;
	}

}

//meetateight
//theyouthevent
//meetateighttheyouthevent
//sentence_len